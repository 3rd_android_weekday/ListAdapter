package com.kshrd.listadapter.recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.kshrd.listadapter.R;
import com.kshrd.listadapter.listview.Article;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecyclerViewActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);


        final List<Article> articleList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Article article = new Article((i+1), "Named -> " + (i+1));

            Random random = new Random();
            int min = 2, max = 3;
            int num = random.nextInt(max - min + 1) + max;

            if (i % num == 0){
                article.setTitle(article.getTitle() + "Cambodia has a population of over 15 million. The official religion is");
            }

            articleList.add(article);
        }

        recyclerView = (RecyclerView) findViewById(R.id.rvNumber);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));

        MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(this, articleList);
        recyclerView.setAdapter(adapter);

    }
}
