package com.kshrd.listadapter.articlelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.kshrd.listadapter.R;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInLeftAnimationAdapter;

public class ArticleListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        RecyclerView rvArticleList = (RecyclerView) findViewById(R.id.rvArticleList);
        rvArticleList.setLayoutManager(new LinearLayoutManager(this));

        ArticleListAdapter articleListAdapter = new ArticleListAdapter(this, DataSource.dataSource());
        rvArticleList.setAdapter(new AlphaInAnimationAdapter(articleListAdapter));

    }
}
