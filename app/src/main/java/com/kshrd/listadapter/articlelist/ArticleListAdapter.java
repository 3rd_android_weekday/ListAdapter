package com.kshrd.listadapter.articlelist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kshrd.listadapter.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by pirang on 5/29/17.
 */

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.MyViewHolder> {

    List<Article> articles;
    Context context;

    public ArticleListAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_item, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Article article = articles.get(position);
        holder.tvTitle.setText(article.getTitle());
        holder.tvPostDate.setText(article.getPostDate());
        holder.tvView.setText(article.getView() + " views");

        Picasso.with(context)
                .load(article.getImageUrl())
                .placeholder(R.drawable.mountain)
                .error(R.mipmap.ic_launcher)
                .into(holder.ivThumbnail);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivThumbnail;
        TextView tvTitle;
        TextView tvPostDate;
        TextView tvView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivThumbnail = (ImageView) itemView.findViewById(R.id.mountain);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvPostDate = (TextView) itemView.findViewById(R.id.tvPostDate);
            tvView= (TextView) itemView.findViewById(R.id.tvView);
        }
    }

}
